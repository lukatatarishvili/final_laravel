<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Restourant;
use App\Adress;

class RestourantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //
        // $restourants = Restourant::all();
        $restourants = DB::table('restourants')
                        ->Join('adresses','adresses.id','=','restourants.address_id')
                        ->select('restourants.id','restourants.name','restourants.image','adresses.address_name')
                        ->get();
                    // dd($restourants);

        return view('admin.restourant.index')->with('restourants', $restourants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $addresses = Adress::all();
        return view('admin.restourant.create')->with('addresses', $addresses);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        // dd($request->file('image_file'));
        $this->validate($request, [
            'name' => 'required',
            'image_file' => 'nullable|image|file'
        ]);
           
        if($request->hasFile('image_file')){
            // get file name with extension
            $fileNameWithExt = $request->file('image_file')->getClientOriginalName();
            // get just filename
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('image_file')->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('image_file')->storeAs('public/image_files', $fileNameToStore);
        }else{
        
            $fileNameToStore = 'NoImage';
        }



        $restourant = new Restourant;
        $restourant->name = $request->input('name');
        $restourant->address_id = $request->input('address_id');
        $restourant->image = $fileNameToStore;
        $restourant->save();
        

        return redirect('/restourants')->with('success','Post created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $addresses = Adress::all();
        $restourant = Restourant::find($id);
        return view('admin.restourant.edit')->with(['restourant' => $restourant, 'addresses'=>$addresses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'image_file' => 'nullable|image|file'
        ]);

        if($request->hasFile('image_file')){
            // get file name with extension
            $fileNameWithExt = $request->file('image_file')->getClientOriginalName();
            // get just filename
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('image_file')->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('image_file')->storeAs('public/image_files', $fileNameToStore);
        }else{
        
            $fileNameToStore = 'NoImage';
        }

        $restourant = Restourant::find($id);
        $restourant->name = $request->input('name');
        $restourant->address_id = $request->input('address_id');
        $restourant->image = $fileNameToStore;
        $restourant->save();

        return redirect('/restourants')->with('success','Restourant updated');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

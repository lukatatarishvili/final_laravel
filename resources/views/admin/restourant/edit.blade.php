@extends('layouts.app')

@section('content')



    <h1 style="text-align: center">Add Restourant</h1>

    <form method="POST" action="{{url('/restourants/'.$restourant->id)}}" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" value="{{$restourant->name}}">
        </div>
        <div class="form-group">
            <select name="address_id" class="form-control" id="address_id">
                @foreach ($addresses as $address)
                        <option value="{{ $address->id }}">{{ $address->address_name }}alio</option>
                    @endforeach

            </select>
        </div>
        <div class="form-group">
            <label for="img">Select image:</label>
            <br>
            <input type="file" id="image_file"  class="image_file" name="image_file" >
        </div>
        

        <input type="submit" class="btn btn-primary" value="submit">
        @csrf
        @method('PUT')
    </form>


@endsection

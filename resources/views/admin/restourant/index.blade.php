@extends('layouts.app')

@section('content')
<h1 style="text-align: center">Restourants</h1>
<a href="restourants/create" class="btn btn-primary" style="margin-bottom: 15px;">Create</a>

    <div class="card">
        
                
        

        <table id="posts-table" class="table table-hover posts-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                
                <th scope="col">Name</th>
                <th scope="col">Address</th>
                <th scope="col">Image</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($restourants as $restourant)
                <tr>
                    <td> {{$restourant->id}}</td>
                    <td> {{$restourant->name}}</td>
                    <td> {{$restourant->address_name}}</td>
                    <td> <div class="center">
                     
                      <img src="../storage/image_files/{{$restourant->image}}" alt="image" width="460" height="345">

                  </div></td>
                    <td><a href="restourants/{{$restourant->id}}/edit" type="button" class="btn btn-primary">Edit</td>  

                    @csrf
                    
                    
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
                
    </div>
        
    
@endsection
